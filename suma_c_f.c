#include <stdio.h>

int sumar (int x, int y){
 printf ("Realizando suma... \n");
 int temp=0;
 temp=x+y;
 return temp;
}

int main (int argc, char const *argv[]){
 int a=0;
 int b=0;
 int c=0;
 printf ("Introduce el valor de a (solo números enteros): \n");
 scanf ("%d", &a);
 printf ("Introduce el valor de b (solo números enteros): \n");
 scanf ("%d", &b);
 
 //c=a+b
 c=sumar(a,b);
 printf ("\t El resultado de %d + %d es: %d \n", a, b, c);
 return 0;
}
