#include <stdio.h>
#ifndef LADO
#define LADO 6
#endif

/*
   0j 1j 2j 3j 4j 5j
6i +  +  +  +  +  +
5i +  +  +  +  +
4i +  +  +  +
3i +  +  +
2i +  +
1i +
*/

int main(int argc, char const *argv[]) {
  int i=0;
  int j=0;
  for (i = LADO; i >= 1; i--) {
    for (j = 0; j < i; j++) {
        printf("+ ");
      }
      printf("\n");
    }
  return 0;
}
