struct auto {
  int precio;
  int nLlantas;
  int nPuertas;
  int modelo;
  char color[20];
  char motor[20];
  float altura;
  float longitud;
};

struct computadora {
  int precio;
  int discoDUro;
  int mRAM;
  char procesador[20];
  float velocidadDeProcesador;
  float largo;
  float ancho;
  float grosor;
};

struct alumno {
  int edad;
  char nombre[60];
  char genero;
  char carrera[25];
  char nCuenta[10];
};

struct equipoDeFutbol {
  int campeonatosGanados;
  char nombre[40];
  char coloresCaracteristicos[100];
  char ciudad[40];
};

struct mascota {
  int nDePatas;
  char nombre[40];
  char color[20];
  char raza[40];
  char tipo[40];
  char alas;
  float largo;
  float altura;
};

struct videoJuego {
  int anoDeEstreno;
  int precio;
  char nombre[40];
  char genero[40];
  char plataforma[20];
  float horasJugables;
};

struct superHeroe {
  int anoDeAparicion;
  char nombre[40];
  char editorial[30];
  char poder[100];
  char colorDeTraje[100];
};

struct planetas {
  int anoDeDescubrimiento;
  char nombre[30];
  float peso;
  float distanciaALSol;
  float diametro;
  float temperatura;
};

struct personaje {
  char nombre[30];
  char rolEnElJuego[40];
  char nombreDelJuego[50];
  char habilidadPrincipal[40];
  char vestimentaCaracteristica[50];
};

struct comic {
  int anoDePublicacion;
  char nombre[40];
  char personajePrincipal[40]
  float precio;
};
