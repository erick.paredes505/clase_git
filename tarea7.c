#include <stdio.h>

int main(int argc, char const *argv[]) {
  int a, b, c1, c2;

  printf("Introduce un entero\n");
  scanf("%d", &a);
  printf("Introduce un segundo entero\n");
  scanf("%d", &b);
  c1=a+b;
  c2=a-b;
  printf("La suma de %d + %d es: %d.\n", a, b, c1);
  printf("La resta de %d -%d es: %d.\n", a, b, c2);
  return 0;
}
