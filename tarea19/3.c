#include <stdio.h>

int main(int argc, char const *argv[]) {
  float estatura[3]={1.56, 1.67, 1.83};
  float promedio=0.00;
  promedio=(estatura[0]+estatura[1]+estatura[2])/3.00;
  printf("Promedio de estaturas es: %.2f metros.\n", promedio);
  return 0;
}
