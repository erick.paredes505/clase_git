#include <stdio.h>

int main(int argc, char const *argv[]) {
  int piezas=0;
  float precio=0.0f;
  float salario=0.0f;
  printf("¿Cuantas piezas se vendieron?\n");
  scanf("%d", &piezas);
  if (piezas<24) {
    precio=450; //precio menudeo
     if (piezas>=1 && piezas<=12) { //salario de 1 a 12 piezas
      salario=(piezas*precio)*0.05;
      printf("El salario del trabajador es: $%.2f\n", salario);
    }
     if (piezas>=13 && piezas<=23) { //salario de 13 a 23 piezas
      salario=(piezas*precio)*0.07;
      printf("El salario del trabajador es: $%.2f\n", salario);
    }
  } else if (piezas>=24) {
    precio=435; //precio mayoreo
     if (piezas>=24 && piezas<=30) { //salario de 24 a 30 piezas
      salario=(piezas*precio)*0.07;
      printf("El salario del trabajador es: $%.2f\n", salario);
    }
     if (piezas>30) { //salario mas de 30 piezas
      salario=(piezas*precio)*0.1;
      printf("El salario del trabajador es: $%.2f\n", salario);
    }
  } else if (piezas<1) {
    printf("No es un valor valido para calcular un salario.\n");
  }
  return 0;
}
