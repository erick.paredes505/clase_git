#include <stdio.h>

int main(int argc, char const *argv[]) {
  printf("Una variable int ocupa %d bytes en memoria\n", sizeof(int));
  printf("Una variable float ocupa %d bytes en memoria\n", sizeof(float));
  printf("Una variable char ocupa %d bytes en memoria\n", sizeof(char));
  printf("Una variable long int ocupa %d bytes en memoria\n", sizeof(long int));
  return 0;
}
