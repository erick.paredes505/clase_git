#include <stdio.h>

int main(int argc, char const *argv[]) {
  float saldo=0.0f;
  float deposito=0.0f;
  float retiro=0.0f;
  int eleccion=0;
  char sino;

  while (eleccion!=4) {
    printf("Elija la opcion acorde al movimiento que desea hacer:\n");
    printf("1. Depositar\n");
    printf("2. Retirar\n");
    printf("3. Donar a Bécalos ($15.00)\n");
    printf("4. Salir\n");
    scanf("%d", &eleccion);
    switch (eleccion) {
      case 1:
       printf("Cuenta con: $%.2f en su cuenta.\n", saldo);
       printf("Ingrese la cantidad a depositar en la cuenta:\n");
       scanf("%f", &deposito);
       printf("El saldo ingresado en la cuenta es: $%.2f\n", deposito);
       saldo=saldo+deposito;
       printf("Ahora cuenta con: $%.2f en su cuenta\n", saldo);
      break;

      case 2:
       printf("Cuenta con: $%.2f en su cuenta.\n", saldo);
       printf("Ingrese la cantidad a retirar en la cuenta:\n");
       scanf("%f", &retiro);
       if (retiro>saldo) {
        printf("No cuenta con saldo suficiente para este movimiento.\n");
       } else {
        printf("El saldo retirado en la cuenta es: $%.2f\n", retiro);
        saldo=saldo-retiro;
        printf("Ahora cuenta con: $%.2f en su cuenta\n", saldo);
       }
      break;

      case 3:
       if (saldo>=15.00) {
         printf("Cuenta con: $%.2f en su cuenta.\n", saldo);
         printf("¿Quiere donar $15.00 a Bécalos?\n");
         printf("y/n\n");
         scanf("%s", &sino);
         switch (sino) {
          case 'y':
           printf("Acaba de donar $15.00 a Bécalos\n");
           saldo=saldo-15.00;
           printf("Ahora cuenta con: $%.2f en su cuenta\n", saldo);
           printf("Muchas gracias.\n");
          break;

          case 'n':
           printf("Regresando a menu principal...\n");
          break;

          default:
           printf("Eleccion no valida.\n");
          break;
        }
      } else {
       printf("No cuenta con saldo suficiente para este movimiento.\n");
      }
     break;

    case 4:
    printf("Vuelva pronto.\n");
    break;

    default:
    printf("Eleccion no valida\n");
    }
  }

  return 0;
}
