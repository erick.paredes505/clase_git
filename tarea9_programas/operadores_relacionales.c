#include <stdio.h>

int main(int argc, char const *argv[]) {
  int var1=5;
  int var2=2;
  int res=0;

  //Igual a
  res=var1==var2;
  printf("%d == %d = %d\n", var1, var2, res);

  //Menor igual que
  res=var1<=var2;
  printf("%d <= %d = %d\n", var1, var2, res);

  //Mayor que
  res=var1>var2;
  printf("%d > %d = %d\n", var1, var2, res);

  //Diferente de
  res=var1!=var2;
  printf("%d != %d = %d\n", var1, var2, res);
  return 0;
}
