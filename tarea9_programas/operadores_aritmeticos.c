#include <stdio.h>

int main(int argc, char const *argv[]) {
  int var1=5;
  int var2=2;
  int res=0;

  //suma
  res=var1+var2;
  printf("%d + %d = %d\n", var1, var2, res);

  //resta
  res=var1-var2;
  printf("%d - %d = %d\n", var1, var2, res);

  //multiplicacion
  res=var1*var2;
  printf("%d * %d = %d\n", var1, var2, res);

  //division
  res=var1/var2;
  printf("%d / %d = %d\n", var1, var2, res);

  //modulo
  res=var1%var2;
  printf("%d mod %d = %d\n", var1, var2, res);
  return 0;
}
