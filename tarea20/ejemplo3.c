#include <stdio.h>
#include <string.h>
#include "alumnos.h"
#define MAX 3

int main(int argc, char const *argv[]) {
/*
  ALUMNO var1;

  var1=nuevoAlumno();

  imprimeAlumno(var1);
*/
  ALUMNO lista[MAX];

  int i=0;
  for (i = 0; i < MAX; i++) {
    printf("Datos alumno %d\n", i+1);
    lista[i]=nuevoAlumno();
  }

  for (i = 0; i < MAX; i++) {
    imprimeAlumno(lista[i]);
  }
  return 0;
}
