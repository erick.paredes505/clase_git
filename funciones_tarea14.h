#include <stdio.h>

void E() {
  printf("EEEEEE\nE\nEEEE\nE\nEEEEEE\n");
}

void R() {
  printf("\nRRRRRR\nR    R\nRRRRRR\nR   R\nR    R\n");
}

void I() {
  printf("\nIIIIII\n  II\n  II\n  II\nIIIIII\n");
}

void C() {
  printf("\nCCCCCC\nC\nC\nC\nCCCCCC\n");
}

void K() {
  printf("\nK  K\nK K\nKK\nK K\nK  K\n");
}
