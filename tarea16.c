#include <stdio.h>
#define TOP 1
#define MAX 5
#define CEN 3

int main(int argc, char const *argv[]) {
  int i=0;
  int j=0;
  /*
  + + + + +
  +       +
  +       +
  +       +
  + + + + +
  */
  for (i = 0; i < MAX; i++) {
    printf("+ ");
  }
  printf("\n");

  for (i = 0; i < CEN; i++) {
    for (j = 0; j < TOP; j++) {
      printf("+");
    }
    printf("       ");
    printf("+");
    printf("\n");
  }

  for (i = 0; i < MAX; i++) {
    printf("+ ");
  }
  printf("\n");

  return 0;
}
